const socket = io('https://boiling-fortress-23587.herokuapp.com/');

var markerLimit = 40
var map
var santaMarker
var santaAddress = "Unknown"
var markers = {
    "marker": [],
    "latitudes": [], 
    "longitudes": []
};
var santaIcon = L.icon({
    iconUrl: 'assets/images/santa.png',
    iconSize: [30, 30]
});

var markerIcon = L.icon({
    iconUrl: 'assets/images/marker.png',
    iconSize: [25, 41]
});

function sendRequest(url, cb) {
    fetch(url)
    .then((response) => {
        response.json()
            .then((value) => {
                cb(value);
            });
    });
}

function getAverage(lat, lgt) {
    const arrAvg = arr => arr.reduce((a,b) => a + b, 0) / arr.length
    return {
        "latitude": arrAvg(lat), 
        "longitude": arrAvg(lgt)
    }
}

function localizeSanta() {
    if (map != undefined && santaMarker != undefined) {
        let lat = santaMarker.getLatLng().lat
        let lng = santaMarker.getLatLng().lng

        // fly to the Santa's house
        map.flyTo(santaMarker.getLatLng())

        // gets Santa address
        sendRequest("http://nominatim.openstreetmap.org/reverse.php?format=json&lat="+lat+"&lon="+lng, (data) => {
            let address = data.address
            if (address != undefined) {
                var now = new Date()
                santaAddress = '<b>county:</b> '+(address.county ? address.county : "Unknown")+'<br>'
                santaAddress += '<b>state:</b> '+(address.state ? address.state : "Unknown")+'<br>'
                santaAddress += '<b>country:</b> '+(address.country ? address.country : "Unknown")+'<br>'
                santaAddress += '<b>updated:</b> '+now.toLocaleTimeString();
            }
        })
    }
}

function updateSantaLocation(localize=false) {
    if (map != undefined) {
        if (santaMarker != undefined) {
            map.removeLayer(santaMarker)
        }
        let average = getAverage(markers.latitudes, markers.longitudes)
        santaMarker = L.marker([average.latitude, average.longitude], {icon: santaIcon}).addTo(map)
        santaMarker.bindPopup(santaAddress).openPopup();
        if (localize) {
            localizeSanta()
        }
    }
}

function initWebsocket() {
    // creates the map
    map = L.map('mapid').setView([48.53, 2.14], 5)
    L.tileLayer('https://{s}.tile.openstreetmap.org/{z}/{x}/{y}.png', {
        maxZoom: 19,
        attribution: ' <a href="http://www.openstreetmap.org/copyright">OpenStreetMap</a>'
    }).addTo(map)

    socket.on('santa', coord => {
        // adds a marker on the coordonate
        if(markers.marker.length == markerLimit) {
            markers.latitudes.shift()
            markers.longitudes.shift()
            map.removeLayer(markers.marker.shift())
        }
        coordJSON = JSON.parse(coord)
        let marker = L.marker([coordJSON.latitude, coordJSON.longitude], {icon: markerIcon}).addTo(map)
        markers.latitudes.push(coordJSON.latitude)
        markers.longitudes.push(coordJSON.longitude)
        markers.marker.push(marker)

        // updates Santa location
        if (santaMarker != undefined && santaAddress != "Unknown") {
            updateSantaLocation()
        } else {
            updateSantaLocation(true)
        }
    });

    window.setInterval(() => {localizeSanta()}, 10000)
}